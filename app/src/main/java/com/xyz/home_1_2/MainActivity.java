package com.xyz.home_1_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/*
2. Создать приложение с одним экраном. Добавить на экран поле для ввода текста (EditText) и две кнопки.
По нажатию на первую кнопку с текстового поля весь введенный текст исчезает. По нажатию на вторую кнопку
поле ввода восстанавливает текст, удаленный первой кнопкой.

 */
public class MainActivity extends AppCompatActivity {


    Button clearButton;
    Button restoreButton;
    EditText textEdit;
    String savedText = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        clearButton = (Button) findViewById(R.id.button_clear);
        restoreButton = (Button) findViewById(R.id.button_restore);
        textEdit = (EditText) findViewById(R.id.editText);


        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savedText = textEdit.getText().toString();
                textEdit.getText().clear();
            }
        });

        restoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textEdit.setText(savedText);
            }
        });


    }
}
